/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EjemploString;

/**
 *
 * @author heltonsmith
 */
public class Principal {
    public static void main(String[] args) {
        String texto = "juanito perez";
        
        char algo = texto.charAt(2);
        System.out.println("Valor: " + algo);
        
        String subCadena = texto.substring(2, 5);
        System.out.println("SubCadena: " + subCadena);
        
        String apellido = texto.substring(8);
        System.out.println("Apellido: " + apellido);
        
        String mayuscula = texto.toUpperCase();
        System.out.println("Mayúsculas: " + mayuscula);
        
        String c1 = "Hola";
        String c2 = "hOlA";
        
        if (c1.equalsIgnoreCase(c2)) {
            System.out.println("Son iguales :D");
        }
        else{
            System.out.println("Son diferentes :(");
        }
        
        if (c1.isEmpty()) {
            System.out.println("Está vacío :(");
        }
        else{
            System.out.println("No está vacío");
        }
        
        String cadena2 = "Chao";
        int l = cadena2.length();
        System.out.println("Largo: " + l);
        
        String cadena3 = "Hola como estás?";
        String r = cadena3.replace('o', '@');
        System.out.println("Reemplazo: " + r);
        
        String cadena4 = "siempre te amé, porque siempre me cuidaste XD";
        String r2 = cadena4.replaceAll("siempre", "nunca");
        System.out.println("Reemplazo: " + r2);
        
        String cadena5 = "¿hola juanito perez cómo te vá?";
        String arreglo[] = cadena5.split(" ");
        System.out.println("Cadena: " + arreglo[5]);
        System.out.println("Cadena: " + arreglo[0]);
        
        System.out.println(cadena5.startsWith("¿holl"));
        System.out.println(cadena5.endsWith("?"));
        
        String cadena6 = "juanito del boom";
        char arregloC[] = cadena6.toCharArray();
        System.out.println(arregloC[1]);
        
        System.out.println(cadena6.contains("uan"));
        
    }
}
